<?php
/**
 * @file
 * Contains the script to be run via process calling php -f command
 */
if (isset($argv[0]) && isset($argv[1]) && isset($argv[2])) {
  $_SERVER['REMOTE_ADDR']    = '';
  $_SERVER['REQUEST_METHOD'] = '';
  $delimiter                 =
    DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . 'all';
  $path_parts                = explode($delimiter, __FILE__);
  define('DRUPAL_ROOT', $path_parts[0]);
  require_once DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $time_started = date('Y_m_d_H_i_s');
  /** @var ExcelDataExport $obj_excel_data_export */
  $obj_excel_data_export = new ExcelDataExport();
  $headers_and_fields = ExcelDataExport::get_headers_and_fields($argv[1]);
  $headers            = $headers_and_fields['headers'];
  $fields             = $headers_and_fields['fields'];
  unset($headers_and_fields);
  $selected_fields = explode(',', $argv[2]);
  $headers_temp    = array();
  $fields_temp     = array();
  foreach ($selected_fields as $value) {
    $headers_temp[$value - 1] = $headers[$value - 1];
    $fields_temp[$value - 1]  = $fields[$value - 1];
  }
  $headers = $headers_temp;
  unset($headers_temp);
  $fields = $fields_temp;
  unset($fields_temp);
  print_r($headers);
  read_console();
  print_r($fields);
  read_console();
  $module_path = drupal_get_path('module', 'excel_data_export');
  include_once DRUPAL_ROOT . '/' . $module_path . '/src/PHPExcel/Classes/PHPExcel.php';
  $emails_entered = isset($argv[3]) ? drupal_strlen($argv[3]) : 0;
  if ($emails_entered) {
    $obj_excel_data_export->process(
      $argv[1], $headers, $fields, $time_started, $module_path,
      $emails_entered
    );
  }
  else {
    $obj_excel_data_export->process(
      $argv[1], $headers, $fields, $time_started, $module_path
    );
  }
  drupal_exit();
}

function read_console() {
  if (PHP_OS == 'WINNT') {
    echo '$ ';
    $line = stream_get_line(STDIN, 1024, PHP_EOL);
  }
  else {
    $line = readline('$ ');
  }
  return $line;
}

/**
 * Class ExcelDataExport
 */
class ExcelDataExport {

  public function process($machine_name, $headers, $fields, $time_started, $module_path, $emails = NULL) {
    $filename      =
    $filename = $time_started . '_' . $machine_name . '_export';
    $absolute_path =
      DRUPAL_ROOT . '/' . $module_path . '/output/' . $filename . '.xlsx';
    $nids          = $this->get_nids($machine_name);
    $current_row   = 1;
    $excel         = $this->get_excel($absolute_path);
    $this->write_headers($absolute_path, $headers, $current_row, $excel);
    $current_row += 1;
    foreach ($nids as $nid) {
      $row_data = $this->get_data($nid, $machine_name, $fields);
      $this->write_row($absolute_path, $current_row, $row_data, $excel);
      $current_row += 1;
      /* print 'Row: ' . ($current_row - 1) . ' || Memory usage: ' . (memory_get_usage(
            TRUE
          ) / 1024 / 1024) . 'MB' . "\n"; */
      // if ($current_row == 101) { break; }
    }
    $this->get_excel_writer($excel)->save($absolute_path);
    /* print 'Memory usage after writing file: ' . (memory_get_usage(
          TRUE
        ) / 1024 / 1024) . 'MB' . "\n"; */
    // read_console();
  }

  private function write_row($absolute_path, $current_row, $row_data, &$excel = NULL) {
    if (is_null($excel)) {
      $excel = $this->get_excel($absolute_path);
    }
    // $excel = $this->get_excel($absolute_path);
    foreach ($row_data as $column => $value) {
      $excel->getActiveSheet()->setCellValueByColumnAndRow(
        $column, $current_row, $value
      );
      $excel->getActiveSheet()->getColumnDimension($this->get_column($column))
            ->setAutoSize(TRUE);
    }
    // $this->get_excel_writer($excel)->save($absolute_path);
  }

  private function write_headers($absolute_path, $headers, $current_row, &$excel = NULL) {
    if (is_null($excel)) {
      $excel = $this->get_excel($absolute_path);
    }
    // $excel = $this->get_excel($absolute_path);
    $current_column = 0;
    $range_start    = NULL;
    $range_end      = NULL;
    foreach ($headers as $header) {
      $cell = $this->get_column_row($current_column, $current_row);
      if (is_null($range_start)) {
        $range_start = $cell;
      }
      $range_end = $cell;
      $excel->getActiveSheet()->setCellValue($cell, trim($header));
      $excel->getActiveSheet()->getStyle($cell)->getFont()->setBold(TRUE);
      $current_column += 1;
    }
    $excel->getActiveSheet()->setAutoFilter("$range_start:$range_end");
    // $this->get_excel_writer($excel)->save($absolute_path);
  }

  private function get_excel($absolute_path) {
    if (!file_exists($absolute_path)) {
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setCreator(
        "Excel data export module"
      );
      $objPHPExcel->getProperties()->setLastModifiedBy(
        "Excel data export module"
      );
    }
    else {
      $objPHPExcel = (new PHPExcel_Reader_Excel2007())->load($absolute_path);
    }
    $objPHPExcel->setActiveSheetIndex(0);
    return $objPHPExcel;
  }

  private function get_excel_writer($excel) {
    return new PHPExcel_Writer_Excel2007($excel);
  }

  private function get_column_row($column, $row) {
    return $this->get_column($column) . $row;
  }

  private function get_column($requested_index) {
    $values        = array(
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    );
    $max_index     = count($values) - 1;
    $current_cycle = NULL;
    $value         = NULL;
    do {
      if ($requested_index > $max_index) {
        is_null($current_cycle) ? $current_cycle = 0 : $current_cycle += 1;
        $requested_index -= $max_index + 1;
      }
    } while ($requested_index > $max_index);
    if (!is_null($current_cycle)) {
      $value = $values[$current_cycle];
    }
    $value = $value . $values[$requested_index];
    return $value;
  }

  private function get_data($nid, $node_machine_name, $fields) {
    $data = array();
    foreach ($fields as $key => $value) {
      $field_machine_name = $value['machine_name'];
      $module             = $value['module'];
      $display_module     = $value['display_module'];
      switch ($module) {
        case 'node_title':
          $sql =
            "SELECT n.`title` AS 'data' FROM node AS n WHERE n.`type` = '{$node_machine_name}' AND n.`nid` = {$nid};";
          break;
        case 'options':
          switch ($display_module) {
            case 'taxonomy':
              $sql =
                "SELECT t.`name` AS 'data' FROM field_data_{$field_machine_name} AS f, taxonomy_term_data AS t WHERE t.tid = f.`{$field_machine_name}_tid` AND f.`entity_id` = {$nid};";
              break;
          }
          break;
        case 'taxonomy':
          $sql =
            "SELECT t.`name` AS 'data' FROM field_data_{$field_machine_name} AS f, taxonomy_term_data AS t WHERE t.tid = f.`{$field_machine_name}_tid` AND f.`entity_id` = {$nid};";
          break;
        case 'email':
          $sql =
            "SELECT f.`{$field_machine_name}_email` AS 'data' FROM field_data_{$field_machine_name} AS f WHERE f.`entity_id` = {$nid};";
          break;
        case 'entityreference':
          $sql =
            "SELECT t.`title` AS 'data' FROM node AS n, field_data_{$field_machine_name} AS f, node AS t WHERE n.`nid` = f.`entity_id` AND f.{$field_machine_name}_target_id = t.`nid` AND n.`nid` = {$nid};";
          break;
        default:
          $sql =
            "SELECT f.`{$field_machine_name}_value` AS 'data' FROM field_data_{$field_machine_name} AS f WHERE f.`entity_id` = {$nid};";
          break;
      }
      if ($sql) {
        $db_statement = db_query($sql);
        $result       = $db_statement->fetchAssoc()['data'];
        if (!isset($result)) {
          $result = '';
        }
        $result     = trim($result);
        $data[$key] = $result;
      }
      else {
        $data[$key] = '';
      }
    }
    $data = array_values($data);
    return $data;
  }

  private function get_nids($node_machine_name) {
    // Get the ID's of the corresponding nodes to export
    $sql          =
      "SELECT n.nid AS 'nid' FROM node AS n WHERE n.`type` = '$node_machine_name';";
    $sql_results  = db_query($sql);
    $export_nodes = array();
    foreach ($sql_results as $sql_result) {
      $export_nodes[$sql_result->nid] = $sql_result->nid;
    }
    return $export_nodes;
  }

  public static function list_node_machine_names() {
    $types       = node_type_get_types();
    $return_data = array();
    foreach ($types as $machine_name => $ob_type) {
      $return_data[(string) $machine_name] = (string) $ob_type->name;
    }
    return $return_data;
  }

  public static function list_node_fields($machine_name) {
    $fields  =
      ExcelDataExport::get_headers_and_fields($machine_name)['headers'];
    $options = array();
    foreach ($fields as $key => $value) {
      $options[(string) ($key + 1)] = (string) $value;
    }
    return $options;
  }

  public static function get_headers_and_fields($node_machine_name) {
    $instances    = field_info_instances('node', $node_machine_name);
    $extra_fields =
      field_info_extra_fields('node', $node_machine_name, 'form');
    $headers[]    = $extra_fields['title']['label'];
    $fields[]     = array(
      'machine_name'   => 'title',
      'module'         => 'node_title',
      'display_module' => '',
    );
    foreach ($instances as $field_machine_name => $value) {
      $headers[] = $value['label'];
      $field     = array(
        'machine_name'   => $field_machine_name,
        'module'         => $value['widget']['module'],
        'display_module' => isset($value['display']['default']['module'])
          ? $value['display']['default']['module'] : '',
      );
      $fields[]  = $field;
    }
    unset($field_machine_name);
    unset($value);
    return array('headers' => $headers, 'fields' => $fields);
  }
}
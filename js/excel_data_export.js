jQuery(function () {
    jQuery(document).ajaxComplete(function () {
        var $type_select = jQuery(':input[name="type"]');
        if ($type_select.val().length) {
            var $select_all = jQuery(':input[name="select_all"]');
            $select_all.change(function () {
                if(jQuery.isFunction($select_all.prop)) {
                    jQuery(':input[name^=fields]').prop('checked', $select_all.prop('checked'));
                } else if(jQuery.isFunction($select_all.attr)) {
                    if($select_all.is(':checked')) {
                        jQuery(':input[name^=fields]').attr('checked', 'checked');
                    } else {
                        jQuery(':input[name^=fields]').removeAttr('checked');
                    }
                }
            });
        }
    });
});